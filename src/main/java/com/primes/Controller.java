package com.primes;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/primes")
public class Controller {
	
	Service serv = new Service();
	
	@GetMapping("/isPrime/{num}")
	public ResponseEntity<String> isItPrime(@PathVariable("num") int num){
		String numStr=String.valueOf(num);
		if(serv.isPrime(num)==true) {
			return ResponseEntity.status(200).body(numStr+" IS prime!");
		}
		return ResponseEntity.status(200).body(numStr+" is NOT prime!");
	}
	
	@GetMapping("/firstNPrimes/{n}")
	public ResponseEntity<String> firstNPrimes(@PathVariable("n") int n){
		return ResponseEntity.status(200).body(serv.firstNPrimes(n));
	}
	
	@GetMapping("/nextPrime/{number}")
	public ResponseEntity<String> nextPrime(@PathVariable("number") int number){
		return ResponseEntity.status(200).body(serv.nextPrime(number));
	}

	
	@GetMapping("/hello")
	public ResponseEntity<String> sayHello(){
		return ResponseEntity.status(200).body("Hello there! Welcome to the GitOps demo!!!");
	}
	
		
	
	@GetMapping("/serverScore/{serverName}")
	public ResponseEntity<Integer> score(@PathVariable("serverName") String serverName){
		int randomNum = ThreadLocalRandom.current().nextInt(1, 5 + 1);
		return ResponseEntity.status(200).body(randomNum);
	}

}
