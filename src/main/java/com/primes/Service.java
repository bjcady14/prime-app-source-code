package com.primes;

public class Service {
	
	public boolean isPrime(int number) {
		if(number==0 || number==1) {return false;}
		int current=2; int mid=(int)(number/2);
		while(current<=mid) {
			if(number%current==0) {
				return false;
			}
			current++;
		}
		return true;
	}
	
	public String firstNPrimes(int n) {
		int count=0; int current=2; String primesList="";
		while(count<n) {
			if(isPrime(current)==true) {
				count++;
				if(count<n) {
					primesList+= String.valueOf(current)+" ";
				} else { 
					primesList+=String.valueOf(current);
				}
			}
			current++;
		}
		return primesList;
	}
	
	public String nextPrime(int n) {
		n++;
		while(isPrime(n)!=true) {if(n%2==0) {n++;} else {n+=2;}}
		return String.valueOf(n);
	}

}
