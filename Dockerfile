FROM openjdk:11

EXPOSE 8080

ARG JARFILE=target/primes_application-0.0.1-SNAPSHOT.jar

ADD ${JARFILE} primes.jar

ENTRYPOINT ["java", "-jar", "primes.jar"]
